
class SoundInput {

  constructor(output) {
    this.buffer = null;
    this.output = output;
  }

  loadMicrophone() {
    var self = this;
    navigator.getUserMedia({audio:true}, 
      function(stream) {
        self.source = context.createMediaStreamSource(stream);
        self.output.setInput(self.source);
      },
      function(e) {
        return;
      })
  }

  loadExternal(url) {
    var request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';

    request.onload = function() {
      context.decodeAudioData(request.response, function(buffer) {
        this.buffer = buffer;
      });
    }
    request.send();
  }

  loadInternal(file) {
    var content = new FileReader();
    var self = this;
    content.onload = function(data) {
      context.decodeAudioData(data.target.result, function(buffer) {
        self.buffer = buffer;
      });
    }
    content.readAsArrayBuffer(file);
  }


  start() {
    this.source.start(0);
  }

  stop() {
    this.source.stop(0);
  }

  play() {
    if (typeof this.source !== 'undefined') {
      this.source.stop(0);
    }
    this.source = context.createBufferSource();
    this.source.buffer = this.buffer;
    this.output.setInput(this.source);
  }

}


class DisplayNode {

  constructor(input) {
    this.analyser = context.createAnalyser();
    this.analyser.connect(context.destination);
    this.analyser.fftSize = config.fftSize;

    if (typeof input !== 'undefined') {
      input.connect(this.analyser);
    }

    this.display = document.createElement('CANVAS');
    this.display.className = 'displayNode';
    document.getElementById('dancefloor').appendChild(this.display);
    this.setSize([config.standardSize[0], config.standardSize[1]]);

    //this.visualize();
  }

  setInput(input) {
    input.connect(this.analyser);
  }

  setSize(size) {
    this.size = size;
    this.display.width = this.size[0];
    this.display.height = this.size[1];
  }

  setFFT(fftSize) {
    this.analyser.fftSize = fftSize;
  }

  visualize() {
    return;
  }
}

  class FreqencyDisplay extends DisplayNode {

    constructor(input) {
      super(input);

      this.display.className += ' frequencyDisplay';
      this.active = true;
      this.context = this.display.getContext('2d');
      this.amplifier = config.standardAmplifier;
    }

    getFrequencyData() {
      this.dataPointCount = this.analyser.frequencyBinCount
      this.dataPoints = new Uint8Array(this.dataPointCount);
      this.analyser.getByteFrequencyData(this.dataPoints);
    }

  }

    class CircleFrequencyDisplay extends FreqencyDisplay {

      constructor(input) {
        super(input);

        this.display.className += ' circleFrequencyDisplay';
      }

      visualize() {
        this.getFrequencyData();
  
        this.context.fillStyle = config.colours[0];
        this.context.fillRect(0, 0, this.display.width, this.display.height);
        this.context.beginPath();
  
        for (let i = 0; i < this.dataPointCount + 1; i++) {
          var pos = i % this.dataPointCount;
          this.context.lineTo(((Math.sin((pos / this.dataPointCount) * Math.PI * 2) + 1) / 4 + 0.25) * this.size[0] + this.dataPoints[pos] * Math.sin(pos / this.dataPointCount * Math.PI * 2) * this.amplifier, ((Math.cos((pos / this.dataPointCount) * Math.PI * 2) + 1) / 4 + 0.25) * this.size[1] + this.dataPoints[pos] * Math.cos(pos / this.dataPointCount * Math.PI * 2) * this.amplifier);
        }
        this.context.stroke();
        var self = this;
        requestAnimationFrame(function(){self.visualize()});
      }

    }

    class BarFrequencyDisplay extends FreqencyDisplay {

      constructor(input) {
        super(input);

        this.display.className += ' barFrequencyDisplay';
      }

      visualize() {
        this.getFrequencyData();
  
        this.context.fillStyle = config.colours[0];
        this.context.fillRect(0, 0, this.display.width, this.display.height);
        this.context.beginPath();
  
        for (let i = 0; i < this.dataPointCount; i++) {
          this.context.lineTo(i * (this.size[0] / this.dataPointCount), this.size[1] - this.dataPoints[i] * this.amplifier);
        }
        this.context.stroke();
        var self = this;
        requestAnimationFrame(function(){self.visualize()});
      }

    }

  class VolumeDisplay extends DisplayNode {

    constructor(input) {
      super(input);

      this.display.className += ' volumeDisplay';
      this.active = true;
      this.context = this.display.getContext('2d');
    }

    visualize() {
      return;
    }
  }

class SoundControl {

  constructor(input) {
    this.input = input;

    this.display = document.createElement('DIV');
    this.display.className = 'soundControl';
    this.display.context = this;
    document.getElementById('controls').appendChild(this.display);

    var micButton = document.createElement('INPUT');
    micButton.className = 'controlButton micButton';
    micButton.setAttribute('type', 'button');
    micButton.setAttribute('value', 'Use Microphone');
    micButton.addEventListener('click', this.loadMicrophone)
    this.display.appendChild(micButton);

    var fileSelector = document.createElement('INPUT');
    fileSelector.className = 'fileSelector';
    fileSelector.setAttribute('type', 'file');
    fileSelector.addEventListener('change', this.loadInternal)
    this.display.appendChild(fileSelector);

    var playButton = document.createElement('INPUT');
    playButton.className = 'controlButton playButton';
    playButton.setAttribute('type', 'button');
    playButton.setAttribute('value', 'Play');
    playButton.addEventListener('click', this.play)
    this.display.appendChild(playButton);

    var stopButton = document.createElement('INPUT');
    stopButton.className = 'controlButton stopButton';
    stopButton.setAttribute('type', 'button');
    stopButton.setAttribute('value', 'Stop');
    stopButton.addEventListener('click', this.stop)
    this.display.appendChild(stopButton);

  }

  loadMicrophone() {
    this.parentNode.context.input.loadMicrophone();
  }

  loadInternal() {
    if (this.files.length > 0) {
      this.parentNode.context.input.loadInternal(this.files[0]);
    }
  }

  stop() {
    this.parentNode.context.input.stop();
  }

  play() {
    this.parentNode.context.input.play();
    this.parentNode.context.input.start();
  }

}
