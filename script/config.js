
var config = {
    standardSize: [
        800,
        500
    ],
    colours: [
        'rgba(255, 255, 255, 0.2)',
        'rgba(255, 255, 255, 0.4)'
    ],
    fftSize: 128,
    standardAmplifier: 0.4
}