
if (!navigator.getUserMedia) {
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
  }

  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  var context = new AudioContext();
  var visualizer = new CircleFrequencyDisplay();
  var input = new SoundInput(visualizer);
  var control = new SoundControl(input);

  visualizer.visualize();